COMANDI PER CONFIGURAZIONE INIZIALE
- git init: Inizializza un repository Git, creando una cartella .git dove Git archivia tutti i dati.
- git status: Mostra lo stato attuale del repository, utile da usare all'inizio e periodicamente per verificare le modifiche.
- git add NomeFile.md: Aggiunge un file specifico all'area di staging.
- git commit -m 'commento': Registra le modifiche nell'area di staging con un messaggio di commento.
- git log: Visualizza la cronologia dei commit, mostrando i messaggi associati a ciascun commit.
- git blame: Identifica l'autore delle ultime modifiche a ogni riga di un file, utile per analizzare la cronologia delle modifiche.

CREAZIONE E GESTIONE BRANCH
- git branch: Elenca i branch esistenti.
- git merge NomeBranch: Unisce due branch.
- git checkout -b NomeBranch: Crea e passa a un nuovo branch.
- git checkout master: Torna al branch principale.
- git checkout <id>: Passa a un punto specifico nella cronologia.
- git branch -d NomeBranch: Elimina un branch.
- git branch -D NomeBranch: Forza l'eliminazione di un branch remoto.
- ls: Mostra lo stato del repository prima del push.
- git push origin main: Invia le modifiche al repository remoto.

VEDERE PASSWORD E NOME ASSEGNATI
- vim .git/config: Apre il file di configurazione Git per vedere le impostazioni.
- vim git: Comando per vedere le impostazioni di Git (forse un errore, dovrebbe essere vim .git/config).

COMANDI DI LETTURA
- git log: Visualizza la cronologia dei commit.
- git log --oneline: Mostra la cronologia dei commit in formato sintetico.
- git diff: Mostra le differenze tra le modifiche non committate e l'ultimo commit.
- git show: Visualizza i dettagli dell'ultimo commit.
- git show <id>: Visualizza i dettagli di un commit specifico.
- git diff <id1> <id2>: Mostra le differenze tra due commit specifici.

PER ELIMINARE UN FILE
- git rm nomeFile: Rimuove un file e richiede un commit per completare l'operazione.
- git commit -m 'commento': Necessario per confermare la rimozione del file.
- git status: Verifica che l'operazione sia stata eseguita correttamente.
- git rm --cached nomeFile: Rimuove un file dall'indicizzazione di Git senza eliminarlo dal disco.
- git restore --staged <NOME FILE>: Annulla l'aggiunta di un file all'area di staging.

COMANDI PER CONFIGURAZIONE NOME E EMAIL
- git config user.name "nome": Imposta il nome dell'utente per il repository corrente.
- git config user.email "email": Imposta l'email dell'utente per il repository corrente.
- git config --global user.name "nome": Applica il nome dell'utente a tutti i repository.
- git config --global user.email "email": Applica l'email dell'utente a tutti i repository.

PER IGNORARE UN FILE O UNA DIRECTORY A GIT
- touch .gitignore: Crea un file .gitignore per specificare i file da ignorare.
- vim .gitignore: Apre il file .gitignore per l'editing.
- git add .gitignore: Aggiunge il file .gitignore all'area di staging.
- git commit -m 'commento': Registra il file .gitignore nel repository.

PER SPOSTARE/RINOMINARE UN FILE
- mv nomeFile nuovoNomeFile: Rinomina un file.
- git add .: Aggiunge tutte le modifiche all'area di staging.
- git commit -m 'commento': Registra le modifiche nel repository.
- git mv nomeFile nuovoNomeFile: Sposta o rinomina un file e tiene traccia delle modifiche senza bisogno di git add.

COMANDI AGGIUNTIVI
- git commit --amend: Modifica l'ultimo commit.
- git commit --sign: Verifica crittograficamente l'autore dei commit.
- git add -p: Aggiunge porzioni di modifiche all'area di staging.
- git reset: Rimuove i commit mantenendo le modifiche ai file.
- git revert: Annulla un commit specifico creando un commit opposto.
- git cherry-pick: Applica un commit specifico da un branch a un altro.
- git clone: Crea una copia locale di un repository esistente.
- git tag: Associa una versione testuale a un punto del codice, utilizzando il versioning semantico.
- git stash: Salva temporaneamente le modifiche non committate.

